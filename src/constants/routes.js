export const LANDING = "/";
export const PRODUCTS = "/products";
export const SIGNIN = "/signin";
export const SIGNUP = "/signup";
export const SIGNOUT = "/signout";
export const ACCOUNT = "/account";
export const ADMIN = "/admin";