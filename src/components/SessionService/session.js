import React, {useEffect, useState} from 'react'
import {withUserService} from '../UserService'
import {useAuthState} from 'react-firebase-hooks/auth';
import {SessionContext} from '../SessionService'

const withSession = Component => {
    const SessionHof = (props) => {
        const fb = props.UserService
        const [userState, initState, errState] = useAuthState(fb.auth); 
        const [privState, setPrivState] = useState();

        useEffect(() => {
            if(!initState){
                fb.getUserPriv().then((res)=>{
                    setPrivState(res);
                })
            }
        })

        console.log("from session Service: ", userState, privState);

        return(
            <SessionContext.Provider value={{userState, initState, errState}}>
                <Component {...{...props, privState, userState, initState, errState}} />
            </SessionContext.Provider> 
        )
    }

    return withUserService(SessionHof)
}

export default withSession;