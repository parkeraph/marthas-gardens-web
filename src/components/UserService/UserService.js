//TODO: Auto capaitalize the name of a new user
import app from "firebase";
import dotenv from "dotenv";
import axios from "axios";
import { promises } from "dns";
dotenv.config();

var FBconfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID
}


class UserService {
    constructor(){
        app.initializeApp(FBconfig);
        this.auth = app.auth();
        const storageRef = app.storage().ref();
        this.imageStorage = storageRef.child('productImages')

        this.backend = axios.create({
            baseURL: process.env.REACT_APP_BACKEND,
            timeout: 4000
          });

        console.log(process.env.REACT_APP_BACKEND);
    }

    getAuthStatus(){
        return this.auth.currentUser;
    }

    createUser(firstName, lastName, newEmail, newPassword){
        return new Promise((resolve, reject) => {
            app.auth().createUserWithEmailAndPassword(newEmail, newPassword).then(creationRes => {
                const newUser = app.auth().currentUser; //user that is now logged in 
                
                //update user profile
                this.updateFirebaseProfile(newUser, firstName, lastName).then(updateRes => {
                    //create record for backend 
                    this.createUserDetailEntry(newUser.uid, firstName+" "+lastName, newEmail).then(res => resolve(res)).catch(err => {
                        console.log(err)
                        this.deleteUser(newUser.uid).catch(error => {
                            console.log("Could not revert!")
                            reject(""+err)
                        })
                        reject(""+ err)
                    })
                })


            }).catch(creationErr =>{
                console.log("Error creating user", creationErr)
                reject(creationErr.message);
            })
        })
    }

    loginUser(email, password){
        return new Promise((resolve, reject) => {
            app.auth().signInWithEmailAndPassword(email, password).then(res => {
               resolve(res);
            }).catch(err => {
                this.signOutUser();
                reject(err);
            })
        })
    }

    signOutUser(){
        return new Promise((resolve, reject) => {
            app.auth().signOut().then(res =>{
                resolve(res);
            }).catch(err => {
                reject(err);
            })
        })
    }

    updateFirebaseProfile(user, firstName, lastName){
        return new Promise((resolve, reject) => {
            user.updateProfile({displayName: firstName+" "+lastName}).then(updateRes => {
                resolve(updateRes);
            }).catch(updateErr => { //error setting name
                console.log("Error setting username", updateErr)
                reject(updateErr);
            })
        })
    }

    //PRIVATE
    //TODO: consolidate these too methods
    createUserDetailEntry(uid, displayName, email){ //every user must have details about them stored via backend API
        return new Promise((resolve, reject) => {
            this.backend({
                method: 'post',
                url: "user/",
                data: {
                  uid: uid,
                  displayName: displayName,
                  email: email
                }
              }).then(res => {
                resolve(res);
            }).catch(err => {
                reject(err)
            })
        })
    }

    updateUserDetailEntry(uObj){  
        return new Promise((resolve, reject) => {
            this.getIdToken();
            this.backend({
                method: "put",
                url: "user/",
                data: {
                    uObj: uObj,
                    uid: this.auth.currentUser.uid,
                    token: this.auth.currentUser.idToken
                }
            }).then(res => resolve(res)).catch(updateErr => reject(updateErr));
        })
    }

    deleteUser(uid){
        return new Promise((resolve, reject) => {
            this.getIdToken();
            this.backend({
                method: 'delete',
                url: 'user/',
                data:{
                    quid: uid,
                    uid: this.auth.currentUser.uid,
                    token: this.auth.currentUser.idToken
                }
            }).then(res => resolve(res)).catch(delErr => reject(delErr))
        })
    }

    getIdToken(){
        return new Promise((resolve, reject) => {
            if(this.auth.currentUser == null){
                reject();
            }
            this.auth.currentUser.getIdToken(true).then(returnedToken => {
                this.auth.currentUser.idToken = returnedToken
                resolve(returnedToken);
            }).catch(err => {
                reject(err)
            })
        })
    }

    getUserPriv(){
        return new Promise((resolve, reject) => {
            this.getIdToken().then(idToken => {
                let rqstObj = {
                    method: "get",
                    url: "user/",
                    params: {
                        uid: this.auth.currentUser.uid,
                        token: idToken
                    }
                }
                this.backend(rqstObj).then(detailRes => {
                    resolve(detailRes.data.data.userType);
                }).catch(detailErr => {
                    console.log(detailErr)
                    reject(detailErr);
                })
            }).catch(err => {
                console.log(err);
                reject("token error: "+err)
            });
        })
    }

    //Rename class to Firebase Service when time calls for it!!
    getProductPhotos(pid){
        return new Promise((resolve, reject) => {
            let productImagesRef = this.imageStorage.child(pid.toString());
            productImagesRef.listAll().then((res) => {
                resolve(res);
            }).catch(err => reject(err));
        });
    }

    uploadPhoto(pid, photo){
        return new Promise( (resolve, reject) => { 
            let productImagesRef = this.imageStorage.child(pid);
            productImagesRef.put(photo).then(res => {
               resolve(); 
            }).catch(err => {reject(err)});

        });
    }

}

export default UserService;