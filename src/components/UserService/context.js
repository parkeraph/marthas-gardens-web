import React from 'react';

const UserServiceContext = React.createContext(null);

export const withUserService = Component=> props => {
   return ( 
      <UserServiceContext.Consumer>
         {UserService => <Component {...props} UserService={UserService} />}
      </UserServiceContext.Consumer> 
   )
}

export default UserServiceContext;