import React from "react";

import "./index.css"

const Tail = () => {
    return(
        <div id="tailContainer">
            <div id="linkContainer">
                <div id="tailItem">
                    <a href="#jobs">Work at Martha's</a>
                </div>
                <div id="tailItem">
                    <a href="#contact">Contact Us</a>
                </div>
                <div id="tailItem">
                    <a href="consumer">Consumer Info</a>
                </div>
            </div>
            <div id="devInfo">
                Developed and Maintained by Parker Hampton, SouthWest Dev LLC
            </div>
        </div>
    )
}

export default Tail;