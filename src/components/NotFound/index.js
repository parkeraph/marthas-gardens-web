import React from 'react';
import "./index.css"
const NotFound = () => {
    return(
       
        <div id="notFoundContainer">
            <div id="notFoundItem">
                <h1>404 Not Found</h1>
                <h3>Looks like the content you are trying to reach is not yet available. This could be do to the current state of construction.</h3>
            </div>
        </div>
        
    )
}

export default NotFound;