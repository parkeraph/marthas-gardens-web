import React from "react"

import "./index.css"

//a component that displays the currently featured product
const Promoted = () => {
    return (
        <div id="promotedContainer">
            <hr />
            <div id="promotedHeader">
                <h1>Featured Products</h1>
                <h3>See whats trending this harvest</h3>
            </div>
            <div id="promotedProductList">

                {/* this will be a generated from product maked as featured*/}
                <div className="productIcon">
                    <img src="http://via.placeholder.com/300x300"/>
                </div>
                <div className="productIcon">
                    <img src="http://via.placeholder.com/300x300"/>
                </div>
                <div className="productIcon">
                    <img src="http://via.placeholder.com/300x300"/>
                </div>
            </div>
        </div>
    )
}

export default Promoted;