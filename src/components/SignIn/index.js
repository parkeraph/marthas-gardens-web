import React, {useState}from "react";
import { Form} from 'react-bootstrap';
import {SignupLink} from '../SignUp';
import {withUserService} from '../UserService'
import * as ROUTES from "../../constants/routes"
import { withRouter } from 'react-router-dom';

import "./index.css"

const INIT_STATE = {
    email: "",
    password: ""
}

const SignIn = (props) => {
    const UserService = props.UserService;
    const [formState, setFormState] = useState(INIT_STATE);
    const [queryErr, setQueryState] = useState(null);
    

    const handleSubmit = (e) =>{
        e.preventDefault()
        UserService.loginUser(formState.email, formState.password).then(res => {
            props.history.push(ROUTES.LANDING) //TODO: change to user's home!
        }).catch(err => setQueryState(err+""));
    }

    const handleChange = (e) => {
        const ID = e.target.id;

        const newState = {
            ...formState,
            [ID]: e.target.value
        }

        setFormState(newState);
    }

    const errPromt = () => {
        if(queryErr){
            return <p id="errPromt">{queryErr}</p>
        }
    }

    return(
        <div id="signInPage">
            <div id="signInContainer">
                <h2 id="signInHeader">Login</h2>
                <hr />
                    <Form onSubmit={handleSubmit}>

                    <Form.Group >
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="Email" id="email" placeholder="example@example.com" onChange={handleChange}/>
                    </Form.Group>

                    <Form.Group >
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="Password" id="password" placeholder="Password" onChange={handleChange}/>
                    </Form.Group>
                    
                    <Form.Row>
                         <button type="submit" id="signInFormButton">Signin</button>
                        {/* <SignUpLink id="SignInLink"/>  */}
                        <SignupLink />
                    </Form.Row>
                    {errPromt()}
                </Form>
            </div>
        </div>
    )
}

export default withRouter(withUserService(SignIn));