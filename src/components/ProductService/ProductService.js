import axios from "axios"
import React from "react"
import { withUserService } from "../UserService";
import ProductContext from "./context"
import { promised } from "q";

class ProductService{
    constructor(UserService){
        this.userService = UserService;
        this.backend = axios.create({
            baseURL: process.env.REACT_APP_BACKEND+"product/",
            timeout: 4000
        })
    } 

    getAllProduct(qsort){
        if(qsort === null){ qsort = "priceHTL"}
        return new Promise((resolve, reject) => {
            this.backend({
                method: 'get',
                data: {
                    qsort: qsort,
                    qpid: "*"
                }
            }).then(resData => resolve(resData.data)).catch(err => reject(err));
        })
    }

    deleteProduct(pid){
        return new Promise((resolve, reject) => {
            this.userService.getIdToken().then(idToken => {
                const delReqObj = {
                    method: "delete",
                    data: {
                        pid: pid,
                        uid: this.userService.auth.currentUser.uid,
                        token: idToken
                    }
                }
                this.backend(delReqObj).then(res => {
                    resolve(res)
                }).catch(delErr => reject(delErr))
                
            })
        })
    }

    addProduct(pObj){
          return new Promise ((resolve, reject) => {

            this.userService.getIdToken().then(idToken => {
                const postReqObj = {
                    method: "post",
                    data: {
                        pObj: pObj,
                        uid: this.userService.auth.currentUser.uid,
                        token: idToken
                    }
                }
                this.backend(postReqObj).then(res => {
                    resolve(res)
                }).catch(addErr => reject(addErr))
            })
        })
    }

    uploadPhoto(){
        
    }

    updateProduct(pObj){
        return new Promise((resolve, reject) => {
            this.userService.getIdToken().then(idToken => {
                const putReqObj = {
                    method: "put",
                    data: {
                        pObj: pObj,
                        uid: this.userService.auth.currentUser.uid,
                        token: idToken
                    }
                }

                this.backend(putReqObj).then(res => {
                    resolve(res)
                }).catch(putErr => reject(putErr))
            })
        })
    }


    generatePid = () => {
        return new Promise((resolve, reject) => {
            axios({
                method:"get",
                url:process.env.REACT_APP_BACKEND+"pid/"
            }).then(newPid => {
                console.log(newPid)
                resolve(newPid.data.data.pid)
            }).catch(err => reject(err));
        })
    }
}

const withProductService = Component => {
    const PSHOF = (props) => {
        const thisProductService = new ProductService(props.UserService);

        return(
            <ProductContext.Provider>
                <Component {...{...props, thisProductService}} />
            </ProductContext.Provider>
        )
    }

    return withUserService(PSHOF);
}

export {withProductService, ProductService};