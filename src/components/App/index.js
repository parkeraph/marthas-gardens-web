import React from "react";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import * as ROUTES from "../../constants/routes"
import { ClipLoader } from 'react-spinners'
import "./index.css";

import NavBar from "../Navigation"
import LandingPage from "../Landing"
import Tail from "../Tail"
import NotFound from "../NotFound"
import Products from "../Products"
import SignIn from "../SignIn"
import SignUp from "../SignUp"
import {withSession} from '../SessionService'
import User from '../User'
import Admin from '../Admin'

import "./index.css"

const AppRouter = (props) => {
    if(props.initState){
        return(
        <div id="spinnerContainer">
            <div id="spinner">
            <ClipLoader size="150" sizeUnit="px"/>
            </div>
        </div>
        )
    }

    return(
        <div id="appContainer">
            <div id="switchContainer">
                <Router>
                <NavBar userState={props.userState} />
                        <Switch>
                            <Route exact path={ROUTES.LANDING} component={LandingPage} />
                            <Route exact path={ROUTES.PRODUCTS} component={Products} />
                            <Route path={ROUTES.SIGNIN} component={SignIn} />
                            <Route path={ROUTES.SIGNUP} component={SignUp} />
                            <Route path={ROUTES.ACCOUNT} component={User} />
                            <Route path={ROUTES.ADMIN} component={Admin} />
                            <Route component={NotFound} />
                        </Switch>
                </Router>
            </div>
            <div id="tailContainer">
                <Tail /> 
            </div>
        </div>
    )
}

export default withSession(AppRouter);