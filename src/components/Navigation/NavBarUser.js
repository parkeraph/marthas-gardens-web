import React from 'react';
import {Navbar, Nav, NavDropdown} from "react-bootstrap";
import "./index.css";
import {withUserService} from '../UserService'
import {LANDING, ACCOUNT} from "../../constants/routes"
import {withRouter} from "react-router-dom"

 const NavBarUser = (props) => { 
    let displayName = props.session.displayName
    const UserService = props.UserService;
    const rt = props.history;

    if(!displayName){
        displayName = "User";
    }

    const doSignout = () => {
        UserService.signOutUser().then(res => {
            rt.push(LANDING)
        }).catch(err => {
            console.log("error on signout: ",err)
            rt.push(LANDING)
        })
    }

    const toAccount = () => {
        rt.push(ACCOUNT);
    }

    return(
        <Navbar bg="white" expand="lg" >
            <Navbar.Brand href="/">
                <h1 id="logoHeader">Martha's Gardens</h1>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                        <Nav.Link href="/products"><span className="navLinkText">Products</span></Nav.Link>
                        <Nav.Link href="/about"><span className="navLinkText">About</span></Nav.Link>
                        <Nav.Link href="/contact"><span className="navLinkText">Contact Us</span></Nav.Link>
                        <div id="dropDownContainer">   
                            <span className="navLinkText">
                                <NavDropdown title={displayName} id="nav-dropdown">
                                    <NavDropdown.Item onClick={toAccount}>Account</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">Orders</NavDropdown.Item>
                                    <NavDropdown.Item onClick={doSignout}>SignOut</NavDropdown.Item>
                                </NavDropdown>
                            </span>
                        </div>
                        <Nav.Link href="#cart"><span id="cartButton">Cart</span></Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default withRouter(withUserService(NavBarUser));