import React from "react";
import {withSession} from '../SessionService';

import "./index.css"
import NavBarUser from './NavBarUser'
import NavBarDefault from './NavBarDefault'
import NavBarAdmin from './NavBarAdmin'

const NavBar = (props) => {
    const session = props.userState;
    const type = props.privState;

    if(session){
            if(type > 0){
                return <NavBarAdmin session={session} />
            }else{
                return <NavBarUser session={session} />
            }
    }else{return <NavBarDefault />}

}

export default withSession(NavBar);