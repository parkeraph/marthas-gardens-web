import React from 'react'
import {Navbar, Nav} from "react-bootstrap";

 const NavBarDefault = () => {
    return(
        <Navbar bg="white" expand="lg" >
        <Navbar.Brand href="/">
            <h1 id="logoHeader">Martha's Gardens</h1>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
                    <Nav.Link href="/products"><span className="navLinkText">Products</span></Nav.Link>
                    <Nav.Link href="/about"><span className="navLinkText">About</span></Nav.Link>
                    <Nav.Link href="/contact"><span className="navLinkText">Contact Us</span></Nav.Link>
                    <Nav.Link href="/signin"><span className="navLinkText">Login</span></Nav.Link>
                    <Nav.Link href="/signup"><span id="signupButton">Signup</span></Nav.Link>
            </Nav>
        </Navbar.Collapse>
        </Navbar>
    )
}

export default NavBarDefault