import React from 'react';
import Assertation from "./assertation";
import Promoted from '../Promoted'

import "./index.css"

const LandingPage = () => {
    const body1 = "Martha’s Gardens Medjool Date Farm was founded in 1990 by Nels Rogers and his wife Martha. The farm originated on a parcel of desert land on the Yuma Mesa overlooking Yuma’s beautiful Gila Valley, located 10 miles east of downtown Yuma."
    const body2 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

    return(
        <div id="LandingContainer">
            <div id="landingHeaderContainer">
                <div id="headerPhoto" className="headerImg">
                    <div id="headerText" className="bgimg">
                        Only Found In Yuma
                    </div>
                </div>
            </div>
            <div id="bodyContainer">
                <Assertation body={body1} headerMain="A Family Owned Business" headerSecondary="Built on Quality over Quantity" imgURL="https://via.placeholder.com/350x250.png"/>
                <Assertation headerMain="Lorem ipsum dolor" headerSecondary="This is dummy text!" imgURL="https://via.placeholder.com/350x250.png" body={body2} type="reverse"/>
                <Promoted />
            </div>
        </div>
    )
}

export default LandingPage;