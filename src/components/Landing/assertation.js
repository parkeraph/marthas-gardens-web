import React from 'react';

import "./assertation.css"


//because the client wants to edit content on the landing page
//this will be a abstraction of entered advert media by the end user
const Assertation = ({headerMain, headerSecondary, body, imgURL, type}) => {
    
    if(type === "reverse"){
        return(
            <div id="assertation">
                <div id="assertationContianer">
                    <div id="assertationHeader">
                        <h1 id="revHeaderMain">{headerMain}</h1>
                        <h2 id="revHeaderSecondary">{headerSecondary}</h2>
                        <hr />
                    </div>
                </div>
                <div id="assertationBody">
                            <div id="revBodyLeft">
                                <img src={imgURL} ></img> 
                            </div>
                            <div id="revBodyRight">
                                {body}
                            </div>
                </div>
            </div>
    )   
    }

    return(
            <div id="assertation">
                <div id="assertationContianer">
                    <div id="assertationHeader">
                        <h1 id="headerMain">{headerMain}</h1>
                        <h2 id="headerSecondary">{headerSecondary}</h2>
                        <hr />
                    </div>
                </div>
                <div id="assertationBody">
                            <div id="bodyLeft">
                               {body} 
                            </div>
                            <div id="bodyRight">
                                 <img src={imgURL} ></img> 
                            </div>
                </div>
            </div>
    )   
}

export default Assertation;