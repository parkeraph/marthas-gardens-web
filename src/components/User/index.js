//Index to the user components
import React, {useState} from "react"
import { withSession } from "../SessionService";
import { withUserService } from "../UserService";

const Reject = () => {
  return(<h1>You must be logged in to do that! </h1>)
}

const UserContainer = (props) => {
    const userState = props.userState;
    const UserService = props.UserService;
    const [tokenState, setTokenState] = useState();

    if(userState !== null){
        UserService.getIdToken().then(res => {
            setTokenState(res);
        });
    }
        
    return(
        <div>
            <h1>Debug Mode</h1>
            <p>UID: {userState.uid}</p>
            <p>Token: {tokenState}</p>
        </div>
    )
}

const UserPage = withUserService(UserContainer);

const User = (props) => {
    const userState = props.userState;

    return userState ? <UserPage userState={userState}/> : <Reject /> 
}

export default withSession(User);