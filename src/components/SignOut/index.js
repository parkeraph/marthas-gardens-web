import React from "react"
import {withRouter} from "react-router-dom"
import {withUserService} from "../UserService"
import {Button, Nav} from 'react-bootstrap'
import {LANDING} from "../../constants/routes"

const SignOut = (props) => {
    const US = props.UserService;

    const doSignOut = () => {
        US.signOutUser().then(res => {
            props.history.push(LANDING);
        })
    }

    return(
        <Nav.Link><span onClick={doSignOut} className="navLinkText">Signout</span></Nav.Link>
    )
}

export default withRouter(withUserService(SignOut));