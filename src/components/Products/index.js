import React from "react" 


import "./index.css"
    
    let productArr = [];

    for(let i =1; i <= 50; i++){
        const prodJSX = (
            <div className="productIcon">
                    <img src="http://via.placeholder.com/300x300"/>
                    <p className="productTitle">Product {i}</p>
            </div>
        )
        
        productArr.push(
            prodJSX
        )
    }

const Product = () => {
    return(
        <div id="productContainer">
            <div id="productHeader">
                <h1>Products</h1>
                <hr />
                <div id="productCatagory">
                    <div className="catagory">Bulk</div>
                    <div className="catagory">Organic</div>
                    <div className="catagory">Specialty</div>
                    <div className="catagory">Other</div>
                </div>
                <hr />
            </div>
            <div id="productList">
                {/* dummy data */}
                {productArr}
            </div>
        </div>
    )
}

export default Product;
