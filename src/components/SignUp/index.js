//TODO: Names are required, email validation 
import React, {useState} from "react";
import { withRouter } from 'react-router-dom';
import {Form, Row, Col, Alert} from 'react-bootstrap';
import * as ROUTES from '../../constants/routes';
import {withUserService} from '../UserService';

import "./index.css"

const initInputState ={
    firstName: "",
    lastName:"",
    email: "",
    password: "",
    retypePassword: "",
    error: null //this error is for input error
}

const SignUp = (props) => {
    const UserService = props.UserService
    const [queryErr, setQueryErr] = useState(null);
    const [inputState,setInputState] = useState(initInputState);

    const handleSubmit = (e) =>{
        e.preventDefault();
        if(!inputState.firstName || !inputState.lastName){
            setQueryErr("First and last name are required")
            return;
        }


        UserService.createUser(inputState.firstName, inputState.lastName, inputState.email, inputState.password).then(res => {
           props.history.push({ pathname: "/empty" });
            props.history.replace(ROUTES.LANDING); //maybe change to the user's account later on? 
        }).catch(err => {
            setQueryErr(err);
        })
    }

    const handleChange = (e) => {
       const ID = e.target.id;
       const val = e.target.value;

        let newVal = {
            ...inputState,
            [ID]: val,
            error: null
        } 
      
        if(newVal.password !== "" && newVal.retypePassword !== ""){
            if(newVal.password !== newVal.retypePassword){
                newVal.error = "Passwords do not match!"
            }else{
                newVal.error = null;
            }
        }

        setInputState(newVal)
       
    }

    const retypePasswordJSX = () => {
        if(inputState.error === "Passwords do not match!"){
            return(
                <Form.Control isInvalid type="Password" id="retypePassword" placeholder="Password*"  onChange={handleChange}/>
            )
        }else {
            return(
                <Form.Control type="Password" id="retypePassword" placeholder="Password*"  onChange={handleChange}/>
            )
        }
    
    }

    const submitButton = () => {
        if(inputState.error !== null){
            return <button type="submit" id="signUpFormButton" disabled >Signup</button>
        }
        
        return <button type="submit" id="signUpFormButton">Signup</button>
        
    }

    const errorPromt = () => {
        if(queryErr){
            return <div id="errPromt"><Alert variant="danger">{queryErr}</Alert></div>
        }
    }

    return(
        <div id="signInPage">
            <div id="signUpContainer">
                <h2 id="signinHeader">SignUp</h2>
                <hr />
                    <Form onSubmit={handleSubmit}>
                    
                    <Form.Group >
                    <Row>
                        <Col>
                        <Form.Control onChange={handleChange} id="firstName" placeholder="First name*" />
                        </Col>
                        <Col>
                        <Form.Control onChange={handleChange} id="lastName" placeholder="Last name*" />
                        </Col>
                    </Row>
                    </Form.Group>

                    <Form.Group >
                        <div className="inputLabelContainer"><Form.Label>Email Address</Form.Label></div>
                        <Form.Control type="Email" id="email" placeholder="example@example.com" onChange={handleChange}/>
                    </Form.Group>

                    <Form.Group >
                        <div className="inputLabelContainer"><Form.Label>Password</Form.Label></div>
                        <Form.Control type="Password" id="password" placeholder="Password" onChange={handleChange}/>
                    </Form.Group>
                    
                    <Form.Group >
                        <div className="inputLabelContainer"><Form.Label>Re-type Password</Form.Label></div>
                        {retypePasswordJSX()}
                    </Form.Group>

                    <Form.Row>
                        <div className="g-recaptcha" data-sitekey="6LcuFrEUAAAAAGDmGHNsYkh-zWMVs-_fJqeXcX68"></div>
                        {submitButton()}
                </Form.Row>
                </Form>
                
                {errorPromt()}

            </div>
        </div>
    )
}

const SignupLink = () => {
    return(
        <p id="signupLinkContainter">Dont have an Accout? Sign up <a href={ROUTES.SIGNUP} id="signupLink">here</a></p>
    )
}

export default withRouter(withUserService(SignUp));
export{
    SignupLink
}