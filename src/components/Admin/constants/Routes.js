export const ADMIN_USER = "/admin/user";
export const ADMIN_PRODUCT = "/admin/product";
export const ADMIN_CONTENT = "/admin/content";
export const ADMIN_ORDER = "/admin/order";