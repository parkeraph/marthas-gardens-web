import React from "react";
import {withRouter} from "react-router-dom";
import * as ROUTER from "../../constants/Routes"
import "./index.css" 

const AdminNav = (props) => {

    const goProduct = () => {
        props.history.push(ROUTER.ADMIN_PRODUCT)
    }

    const goOrder = () => {
        props.history.push(ROUTER.ADMIN_ORDER)
    }

    const goUser = () => {
        props.history.push(ROUTER.ADMIN_USER)
    }

    const goContent = () => {
        props.history.push(ROUTER.ADMIN_CONTENT)
    }

   return(
    <div id="AdminNavContainer">
        <h1>Admin Panel</h1>
        <hr />
        <div id="productCatagory">
            <div className="catagory" onClick={goProduct}>Product</div>
            <div className="catagory" onClick={goOrder}>Orders</div>
            <div className="catagory" onClick={goUser}>Users</div>
            <div className="catagory" onClick={goContent}>Content</div>
        </div>
        <hr />
    </div>
   )
}

export default withRouter(AdminNav)