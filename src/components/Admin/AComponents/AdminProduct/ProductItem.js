import React, {useState, useEffect} from "react";
import {Alert, Button, Modal, InputGroup, FormControl, Dropdown, DropdownButton} from 'react-bootstrap';
import {withProductService} from "../../../ProductService"
import ImageUploader from "react-images-upload"
import {withUserService} from "../../../UserService";

const ProductItem = (props) => {
    const UserService = props.UserService
    const productService = props.thisProductService
    const productData = props.productData
    const initFileList = [];
    let fileList = [];

    //states
    const [editState, setEditState] = useState(productData);
    const [photoEditState, setPhotoEditState] = useState([]);
    const [editModalShow, setEditModalShow] = useState();
    const [deleteModalShow, setDeleteModalShow] = useState();
    const [photoModalShow, setPhotoModalShow] = useState();
    const [refresh, setRefresh] = useState(false);
    const [errState, setErrState] = useState("");

    useEffect(() => {
        fileList = UserService.getProductPhotos(productData.pid);
        fileList.then(res => {
            console.log("for"+productData.pid,res);
            for(let i = 0; i < fileList.prefixes; i++){
                initFileList.push(fileList.prefixes[i].name)
            }
            setPhotoEditState(initFileList);
        })
    }, [])

    //func
    const handleEditClose = () => setEditModalShow(false);
    const handleEditShow = () => setEditModalShow(true);

    const handlePhotoClose = () => setPhotoModalShow(false);
    const handlePhotoShow = () => setPhotoModalShow(true);

    const handleDeleteClose = () => setDeleteModalShow(false);
    const handleDeleteShow = () => setDeleteModalShow(true);

    const handleUpload = () => {
     setRefresh(!refresh);   
    }

    

    const handleSave = () => {
        if(photoEditState.length !== initFileList.length){
            
        }
        productService.updateProduct(editState).then(res => {
            setEditModalShow(false);
            document.location.reload(true);
        }).catch(err => {
            setErrState(err.message);
        })
    };


    const handleChange = (e) => {
        const val = e.target.value;
        const id = e.target.id;
        let newEditState = {};

        if(val === undefined){
            newEditState = {
                ...editState,
                featured: id
            }
        }else{
            newEditState = {
                ...editState,
                [id]: val
            }
        }
        setEditState(newEditState)
    };

    const handleDrop = (e) => { //one at a time 
        const newEditState = photoEditState;
        for(let i = 0; i < e.target.files.length; i++){
            newEditState.push(e.target.files[i]);
        }
        console.log(newEditState)
        setPhotoEditState(newEditState);
    }

    const handleDelete = () => {
        productService.deleteProduct(productData.pid).then(res => {
            setDeleteModalShow(false);
            document.location.reload(true);
        }).catch(err => {
            setErrState(err.message);
        })
    };

    const photoClick = (e) => {
        e.value = null;
    }

    const saveButton = () => {
        if(editState === props.productData){
           return <Button className="productItemButton" variant="dark" onClick={handleEditShow} disabled>Save</Button>
        }else{
            return <Button className="productItemButton" variant="dark" onClick={handleEditShow}>Save</Button>
        }
    };

    const errDisp = () => {
        if(errState){
            return <Alert variant="danger">{errState}</Alert>
        }
    }

    const fileDisp = photoEditState.map(thisFile => {
            return <><span>{thisFile.name}</span><br /></>
    })

    return(
        <>
        <div className="productItem">
            <div className="infoContainer">
            
                <div className="infoItem">
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon1">PID: </InputGroup.Text>
                        </InputGroup.Prepend>
                            <FormControl
                                id="pid"
                                value={editState.pid}
                                onChange={handleChange}
                                aria-label="PID"
                                aria-describedby="basic-addon2"
                            />
                        
                    </InputGroup>
                </div>
                
                <div className="infoItem">
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon1">Name: </InputGroup.Text>
                        </InputGroup.Prepend>
                            <FormControl
                                id="displayName"
                                value={editState.displayName}
                                onChange={handleChange}
                                aria-label="Display Name"
                                aria-describedby="basic-addon1"
                            />
                        
                    </InputGroup>
                </div>

                <div className="infoItem">
                    <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                id="price"
                                value={editState.price}
                                onChange={handleChange}
                                aria-label="price"
                                aria-describedby="basic-addon1"
                            />
                        
                    </InputGroup>
                </div>
                
                <div className="infoItem">
                    <InputGroup className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="basic-addon1">Quantity:s</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl
                                id="quantity"
                                value={editState.quantity}
                                onChange={handleChange}
                                aria-label="quantity"
                                aria-describedby="basic-addon1"
                            />
                        
                    </InputGroup>
                </div>

                <div className="infoItem">
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon1">Featured: </InputGroup.Text>
                        </InputGroup.Prepend>
                        <DropdownButton
                            id="featured"
                            title={editState.featured}
                            variant="secondary"
                            >
                            <Dropdown.Item onClick={handleChange} id="true" href="#">True</Dropdown.Item>
                            <Dropdown.Item onClick={handleChange} id="false">False</Dropdown.Item>
                        </DropdownButton>
                    </InputGroup>
                </div>
                <div id="infoItem">
                    <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="basic-addon1">Description</InputGroup.Text>
                                </InputGroup.Prepend>
                                <FormControl
                                    id="desc"
                                    value={editState.desc}
                                    onChange={handleChange}
                                    aria-label="price"
                                    aria-describedby="basic-addon1"
                                />
                            
                    </InputGroup>
                </div>  
            </div>
       
            <div className="productItemButtonContainer">
                <div>
                    {errDisp()}
                    <Button className="productItemButton" variant="dark" onClick={handleEditShow}>Save</Button>
                </div>
                <div>
                    <Button className="productItemButton" variant="dark" onClick={handlePhotoShow}>Photos</Button>
                </div>
                <div>
                    <Button className="productItemButton" variant="danger" onClick={handleDeleteShow}>Delete</Button>
                </div>
            </div>
        </div>

        
        {/*Edit modal */}
        <Modal show={editModalShow} onHide={handleEditClose}>
                <Modal.Header closeButton>
                <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>You cannot revert your changes</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleEditClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleSave}>
                    Save Changes
                </Button>
                </Modal.Footer>
        </Modal>

        {/*Photo Modal*/}
        <Modal show={photoModalShow} onHide={handlePhotoClose}>
                <Modal.Header closeButton>
                <Modal.Title>Product Photo Upload</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="dropBox">
                        <input type="file" onClick={photoClick} onChange={handleDrop} multiple/>
                        
                        <div className="fileList">
                            {fileDisp}
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleUpload}>
                    Upload
                </Button>
                <Button variant="dark" onClick={handlePhotoClose}>
                    Done
                </Button>
                </Modal.Footer>
        </Modal>

        {/*delete confirmation modal */}
        <Modal show={deleteModalShow} onHide={handleDeleteClose}>
                <Modal.Header closeButton>
                <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>Once the user is deleted, their account cannot be recovered</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleDeleteClose}>
                    Cancel
                </Button>
                <Button variant="danger" onClick={handleDelete}>
                    Delete
                </Button>
                </Modal.Footer>
        </Modal>
        </>
    )
}

export default withUserService(withProductService(ProductItem));