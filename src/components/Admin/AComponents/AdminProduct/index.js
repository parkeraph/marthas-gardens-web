import React, {useState, useEffect} from "react";
import {Alert, Button, Modal,InputGroup, FormControl, Dropdown, DropdownButton} from 'react-bootstrap';
import axios from "axios"
import ProductItem from "./ProductItem"
import {withProductService} from "../../../ProductService"
import "./index.css"

const DEFUALT_INPUT_STATE = {
    featured: "false"
}

const DEFAULT_FORM_VALID_STATE = { //hold error state for each field if is invalid
    PID: false,
    displayName: false,
    price: false,
    featured: false,
    desc: false
}

const getAllProduct = () => {
        return new Promise((resolve, reject) => {
            const queryBody = {
                qsort:"nameAZ",
                qpid: "*"
            }
            
            axios({
                method: "get",
                url: process.env.REACT_APP_BACKEND+"product/",
                params: queryBody
            }).then(res => {
                console.log(res)
                resolve(res.data.data)
            }).catch(err => {
                reject(err)
            })

        })
    }

const AdminProduct = (props) => {

    const [newModalShow, setNewModalShow] = useState();
    const [productItemState, setProductItemState] = useState([]);
    const [errState, setErrState] = useState("");
    const [editState, setEditState] = useState(DEFUALT_INPUT_STATE);
    const [formValidationState, setFormValidationState] = useState(DEFAULT_FORM_VALID_STATE);

    useEffect(() => {
        getAllProduct().then(res => {
            setProductItemState(res)
        }).catch(err => {
            setErrState(err);
        })
    }, [])

    const handleNewClose = () => setNewModalShow(false)
    const handleNewShow = () => setNewModalShow(true)

    const productElements = productItemState.map(thisElement => {
        return <ProductItem key={thisElement.pid} productData={thisElement} />
    })

    const alert = () => {
        if(errState){
            return <Alert variant="danger">{errState}</Alert>
        }
    }

    /* ~~~functionalty for new product entry~~~ */

    const productService = props.thisProductService;

    const handleSave = (e) => {

        productService.addProduct(editState).then(res => {
            setEditState(DEFUALT_INPUT_STATE);
            document.location.reload(true);
        }).catch(err => {
            console.log(err.message);
            setErrState(err.message);
        });
    }

    const handleChange = (e) => {
        let id = e.target.id;
        let val = e.target.value;
        
        if(val === undefined){
            val = id;
            id = "featured"
        }

        const newPObj = {
            ...editState,
            [id]: val
        }

        

        setEditState(newPObj);
        console.log(newPObj)
        console.log(formValidationState);
    }

    const errDisp = () => {
        if(errState !== ""){
            return <Alert variant="danger">{errState}</Alert>
        }
    }

    const getPid = () => {
        productService.generatePid().then(res => {
            const newPID = res.toString();
            const newPObj = {
                ...editState,
                PID: newPID
            } 

            setEditState(newPObj);
            console.log(editState);
        })
    }

    

    return(
        <div>
        <div id="AdminProductContainer">
            <div id="AdminProductHeader">
                <div id="newProductButtonContainer">
                    <Button variant="dark" onClick={handleNewShow}><b>+</b></Button>
                </div>
                <div id="notice">
                    {alert()}
                </div>
            </div>
            <div id="ProductListContainer">
                {productElements}
            </div>
        </div>

         {/*new product*/}
         <Modal show={newModalShow} onHide={handleNewClose}>
                <Modal.Header closeButton>
                <Modal.Title>New Product Entry</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                   
                    <div className="infoContainer">
                        <div className="infoItem">
                            <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>PID:</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        id="PID"
                                        onChange={handleChange}
                                        value={editState.PID}
                                        aria-label="Name"
                                        aria-describedby="basic-addon1"
                                    />
                                    <InputGroup.Append>
                                        <Button  onClick={getPid}>Generate</Button>
                                    </InputGroup.Append>
                            </InputGroup>
                        </div>

                        <div className="infoItem">
                            <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Title:</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        id="displayName"
                                        value={editState.displayName}
                                        onChange={handleChange}
                                        aria-label="Title"
                                        aria-describedby="basic-addon2"
                                    />
                                
                            </InputGroup>
                        </div>

                        <div className="infoItem">
                            <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Price:</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        id="price"
                                        value={editState.price}
                                        onChange={handleChange}
                                        aria-label="Price"
                                        aria-describedby="basic-addon2"
                                    />
                                
                            </InputGroup>
                        </div>

                        <div className="infoItem">
                            <InputGroup className="mb-3">
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>Quantity:</InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        id="quantity"
                                        value={editState.quantity}
                                        onChange={handleChange}
                                        aria-label="Quantity"
                                        aria-describedby="basic-addon2"
                                    />
                                
                            </InputGroup>
                        </div> 

                        <div className="infoItem">
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Text>Featured:</InputGroup.Text>
                                </InputGroup.Prepend>
                                <DropdownButton
                                    id="featured"
                                    variant="secondary"
                                    title={editState.featured}
                                    >
                                    <Dropdown.Item onClick={handleChange} id="true" href="#">true</Dropdown.Item>
                                    <Dropdown.Item onClick={handleChange} id="false">false</Dropdown.Item>
                                </DropdownButton>
                            </InputGroup>
                        </div>

                        <div className="infoItem">
                            <textarea className="form-control" placeholder="Product Description" id="exampleFormControlTextarea1" rows="3" onChange={handleChange} id="desc" value={editState.desc}></textarea>
                        </div>
                            
                    </div>

                </Modal.Body>
                <Modal.Footer>
                {errDisp()}  
                <Button variant="secondary" onClick={handleNewClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleSave}>
                    Create
                </Button>
                </Modal.Footer>
        </Modal>
        </div>
    )
}

export default withProductService(AdminProduct)