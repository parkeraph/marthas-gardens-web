import React from "react";


const Blocker = () => {
    return(
        <div>
            <h2>Authorization Denied</h2>
            <p>Your account is not authorized to access this page.</p>
        </div>
    )
}

export default Blocker;