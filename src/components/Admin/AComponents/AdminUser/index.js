import React, {useState, useEffect} from "react";
import {withUserService} from "../../../UserService/"
import axios from "axios"
import UserItem from "./UserItem"

import "./index.css"

const getAllUsers = (uid, token) => {
    return new Promise( (resolve, reject) => {
        const queryParams = {
            uid: uid,
            token: token,
            quid: '*'
        }
  
        axios({
            method: "get",
            url: process.env.REACT_APP_BACKEND+"user/",
            params: queryParams
        }).then(res => resolve(res)).catch(err => reject(err));
    })
}



const AdminUser = (props) => { 
    const userService = props.UserService;
    const [userListState, setUserStateList] = useState([])

    console.log(process.env.REACT_APP_BACKEND)

    useEffect(() => {
        if(userService.getAuthStatus().idToken ===  undefined){
            userService.getIdToken().then(res => {
                const user = userService.getAuthStatus()
                getAllUsers(user.uid, user.idToken).then(res => {
                    setUserStateList(res.data.data);
                })
            })
        }else{
            userService.getIdToken().then(res => {
                const user = userService.getAuthStatus()
                getAllUsers(user.uid, user.idToken).then(res => {
                    setUserStateList(res.data.data);
                })
            })
        }
    },[])

    console.log(userListState)

    const userElements = userListState.map(thisElement => {
        return <UserItem key={thisElement.uid} userData={thisElement} />
    })

    return(
        <div id="AdminUserContainer">
            <div id="UserListContainer">
                {userElements}
            </div>
        </div>
    )
}

export default withUserService(AdminUser)