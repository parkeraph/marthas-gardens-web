import React, {useState} from 'react';
import {Button, Modal, InputGroup, FormControl, Dropdown, DropdownButton} from 'react-bootstrap';
import { withUserService } from '../../../UserService';

const userTypeDic = ["Consumer", "Employee", "Admin"];

const UserItem = (props) => {
    //props
    const userService = props.UserService;
    const displayName = props.userData.displayName;
    const userType = props.userData.userType;
    const email = props.userData.email;

    //states
    const [editModalShow, setEditModalShow] = useState();
    const [deleteModalShow, setDeleteModalShow] = useState();
    const [editState, setEditState] = useState(props.userData);
    const [errState, setErrState] = useState("");

    //functions
    const handleEditClose = () => setEditModalShow(false);
    const handleEditShow = () => setEditModalShow(true);

    const handleDeleteClose = () => setDeleteModalShow(false);
    const handleDeleteShow = () => setDeleteModalShow(true);

    const handleSave = () => {
        userService.updateUserDetailEntry(editState).then( res => {
            setEditModalShow(false);
            document.location.reload(true);
        }).catch(err => {
            setErrState(err)
        })
    }

    const handleChange = (e) => {
        const id = e.target.id;
        const val = e.target.value;
        let newEditState = {};

        if(val == undefined){
             newEditState = {
                ...editState,
                userType: userTypeDic.indexOf(id) 
            }
        }else{
             newEditState = {
                ...editState,
                [id]: val
            }
        }

        setEditState(newEditState);
    }
    
    const handleDelete = () => {
        userService.deleteUser(props.userData.uid).then(res => {
            setDeleteModalShow(false);
            document.location.reload(true);
        }).catch(delErr => {
            setErrState(delErr)
        })
    }

    const saveButton = () => {
        if(editState === props.userData){
           return <Button className="userItemButton" variant="secondary" onClick={handleEditShow} disabled>Save</Button>
        }else{
            return <Button className="userItemButton" variant="secondary" onClick={handleEditShow} >Save</Button>
        }
    } 

    return(
        <>
        <div className="userItem ">
            <div className="infoContainer">
                <div className="infoItem">
                    <InputGroup className="mb-3">
                    
                            <FormControl
                                id="displayName"
                                value={editState.displayName}
                                onChange={handleChange}
                                aria-label="Name"
                                aria-describedby="basic-addon1"
                            />
                        
                    </InputGroup>
                </div>

                <div className="infoItem">
                    <InputGroup className="mb-3">
                        
                            <FormControl
                                id="email"
                                value={editState.email}
                                onChange={handleChange}
                                aria-label="Email"
                                aria-describedby="basic-addon2"
                            />
                        
                    </InputGroup>
                </div>

                <div className="infoItem">
                    <InputGroup>
                        <DropdownButton
                            id="userType"
                            variant="secondary"
                            title={userTypeDic[editState.userType]
                            }
                            >
                            <Dropdown.Item onClick={handleChange} id="Consumer" href="#">Consumer</Dropdown.Item>
                            <Dropdown.Item onClick={handleChange} id="Employee">Employee</Dropdown.Item>
                            <Dropdown.Item onClick={handleChange} id="Admin">Admin</Dropdown.Item>
                        </DropdownButton>
                    </InputGroup>
                </div>
                              
            </div>
       
            <div className="userItemButtonContainer">
                <div>
                    {saveButton()}
                </div>
                <div>
                    <Button className="userItemButton" variant="danger" onClick={handleDeleteShow}>Delete</Button>
                </div>
            </div>
        </div>
        
        {/*Edit modal */}
        <Modal show={editModalShow} onHide={handleEditClose}>
                <Modal.Header closeButton>
                <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>The changes cannot be reverted <br />{errState}</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleEditClose}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleSave}>
                    Save Changes
                </Button>
                </Modal.Footer>
        </Modal>

        {/*delete confirmation modal */}
        <Modal show={deleteModalShow} onHide={handleDeleteClose}>
                <Modal.Header closeButton>
                <Modal.Title>Are you sure?</Modal.Title>
                </Modal.Header>
                <Modal.Body>Once the user is deleted, their account cannot be recovered</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleDeleteClose}>
                    Cancel
                </Button>
                <Button variant="danger" onClick={handleDelete}>
                    Delete
                </Button>
                </Modal.Footer>
        </Modal>
        </>
    )

}

export default withUserService(UserItem);