import React, {useState, useEffect} from "react";
import withUserService from "../../../UserService"

//props: base folder for storage endpoint
//       
const AdminPicture = (props) => {
    const baseUrl = props.baseUrl;

    const [photoState, setPhotoState] = useState([]);

    const handSave = () => {

    }

    const handleChange = (e) => {
        console.log(e.target.files)
    }

    return(
        <div id="AdminPictureContainer">
            <div id="pictureInputContainer">
                <input type="file" onChange={handleChange} multiple />
            </div>
        </div>
    )
}

export default withUserService(AdminPicture)