import React from "react"
import {withSession} from "../SessionService"
import Blocker from "./AComponents/Blocker"
import AdminNav from "./AComponents/AdminNav/"
import AdminProduct from "./AComponents/AdminProduct/"
import AdminUser from "./AComponents/AdminUser/"
import AdminContent from "./AComponents/AdminContent/"
import AdminOrder from "./AComponents/AdminOrder/"
import { ClipLoader } from 'react-spinners'
import { BrowserRouter as AdminRouter, Route, Switch} from "react-router-dom";
import * as ROUTES from "./constants/Routes"

const Admin = (props) => {
    const session = props.userState;
    const type = props.privState;

    if(type == null){
        return(
        <div id="spinnerContainer">
            <div id="spinner">
            <ClipLoader size="150" sizeUnit="px"/>
            </div>
        </div>
        )
    }

    if(session && type > 0){
        return(
            <AdminApp />
        )
    }else{
        return(
            <Blocker />
        )
    }

}

const AdminApp = () => {
    return(
        <div id="AdminAppContainer">
            <AdminRouter>
                <AdminNav />
                <Switch>
                    <Route path={ROUTES.ADMIN_USER} component={AdminUser} />
                    <Route path={ROUTES.ADMIN_PRODUCT} component={AdminProduct} />
                    <Route path={ROUTES.ADMIN_CONTENT} component={AdminContent} />
                    <Route path={ROUTES.ADMIN_ORDER} component={AdminOrder} />
                </Switch>
            </AdminRouter>
            
        </div>
    )
}

export default withSession(Admin);