import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppRouter from './components/App';
import UserService, { UserServiceContext } from './components/UserService';


ReactDOM.render(
    <UserServiceContext.Provider value={new UserService()}>
        <AppRouter />
    </UserServiceContext.Provider>, 
    document.getElementById('root'));



